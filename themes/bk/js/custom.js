$( document ).ready(function() {
    $('.owl-carousel').owlCarousel({
        loop:false,
	margin:10,
	nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            }
        }
    });
    $(document).on('change','#download-pdf-files',function(){
        $('#download-printable-pdf').attr('href',$(this).val());
    });
    $(document).on('click','#download-printable-pdf', function(){
        $('#download-printable-pdf').attr('href','#');
    });
    $(document).on('click','#question', function(){
        if($('input[name=question]:checked').val()=='Yes'){
            $('#answer').css('display','block');
          }else{
            $('#answer').css('display','none');
         }
    });
    //question
});
