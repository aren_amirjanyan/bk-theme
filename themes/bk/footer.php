<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */
?>
</div>
<footer>
    <div class="container">
        <?php
        get_template_part( 'template-parts/footer/footer', 'widgets' );

        if ( has_nav_menu( 'social' ) ) : ?>
            <nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'bk' ); ?>">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'social',
                    'menu_class'     => 'social-links-menu',
                    'depth'          => 1,
                    'walker' => new BK_Walker_Social_Nav_Menu()
                ) );
                ?>
            </nav><!-- .social-navigation -->
        <?php endif;
        get_template_part( 'template-parts/footer/site', 'info' );
        ?>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
