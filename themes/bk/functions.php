<?php
/**
 * BK functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 */

/**
 * BK only works in WordPress 4.7 or later.
 */

function bk_setup()
{
    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus(array(
        'top' => __('Top Menu', 'bk'),
        'social' => __('Social Links Menu', 'bk'),
    ));
    // Add theme support for Custom Logo.
    add_theme_support('custom-logo', array(
        'width' => 125,
        'height' => 60,
        'flex-width' => true,
    ));
}

add_action('after_setup_theme', 'bk_setup');

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bk_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Sidebar', 'bk' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'bk' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 1', 'bk' ),
        'id'            => 'sidebar-2',
        'description'   => __( 'Add widgets here to appear in your footer.', 'bk' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 2', 'bk' ),
        'id'            => 'sidebar-3',
        'description'   => __( 'Add widgets here to appear in your footer.', 'bk' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'bk_widgets_init' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function bk_pingback_header()
{
    if (is_singular() && pings_open()) {
        printf('<link rel="pingback" href="%s">' . "\n", get_bloginfo('pingback_url'));
    }
}

add_action('wp_head', 'bk_pingback_header');
/**
 * Register custom fonts.
 */
function bk_fonts_awesome_url()
{
    $fonts_url = '';

    /**
     * Translators: If there are characters in your language that are not
     * supported by Libre Franklin, translate this to 'off'. Do not translate
     * into your own language.
     */
    $libre_franklin = _x('on', 'Libre Franklin font: on or off', 'bk');

    if ('off' !== $libre_franklin) {
        $font_families = array();

        $font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

        $query_args = array(
            'family' => urlencode(implode('|', $font_families)),
            'subset' => urlencode('latin,latin-ext'),
        );

        $fonts_url = add_query_arg($query_args, get_stylesheet_directory_uri() . '/font-awesome/css/font-awesome.min.css');
    }

    return esc_url_raw($fonts_url);
}

function bk_owl_carousel_url()
{
    $bk_owl_carousel_url = add_query_arg([], get_stylesheet_directory_uri() . '/owlcarousel/assets/owl.carousel.min.css');
    return esc_url_raw($bk_owl_carousel_url);
}

function bk_owl_defauult_url()
{
    $bk_owl_defauult_url = add_query_arg([], get_stylesheet_directory_uri() . '/owlcarousel/assets/owl.theme.default.min.css');
    return esc_url_raw($bk_owl_defauult_url);
}

function bk_normalize_url()
{
    $bk_normalize_url = add_query_arg([], get_stylesheet_directory_uri() . '/css/normalize.css');
    return esc_url_raw($bk_normalize_url);
}

function bk_fonts_url()
{
    $bk_fonts_url = add_query_arg([], get_stylesheet_directory_uri() . '/css/fonts.css');
    return esc_url_raw($bk_fonts_url);
}

function bk_responsive_url()
{
    $bk_responsive_url = add_query_arg([], get_stylesheet_directory_uri() . '/css/responsive.css');
    return esc_url_raw($bk_responsive_url);
}

function bk_scripts()
{
    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style('bk-fonts-awesome', bk_fonts_awesome_url(), array(), null);
    wp_enqueue_style('bk-owl-carousel', bk_owl_carousel_url(), array(), null);
    wp_enqueue_style('bk-owl-default', bk_owl_defauult_url(), array(), null);
    wp_enqueue_style('bk-normalize', bk_normalize_url(), array(), null);
    wp_enqueue_style('bk-fonts', bk_fonts_url(), array(), null);
    wp_enqueue_style('bk-style', get_stylesheet_uri());
    wp_enqueue_style('bk-responsive', bk_responsive_url(), array(), null);
}
add_action('wp_enqueue_scripts', 'bk_scripts');

function bk_footer_scripts(){
    wp_enqueue_script( 'bk-jquery', get_theme_file_uri( '/js/jquery-3.1.1.min.js' ));
    wp_enqueue_script( 'bk-custom', get_theme_file_uri( '/js/custom.js' ));
    wp_enqueue_script( 'bk-owl-carousel', get_theme_file_uri( '/owlcarousel/owl.carousel.min.js'));
}
add_action('wp_footer', 'bk_footer_scripts');

add_filter('admin_init', 'register_fields');
function register_fields()
{
    register_setting('general', 'admin_phone', 'esc_attr');
    register_setting('general', 'admin_address', 'esc_attr');
    add_settings_field('admin_phone_field', '<label for="admin_phone">' . __('Admin Phone', 'admin_phone') . '</label>', 'admin_phone_html', 'general');
    add_settings_field('admin_address_field', '<label for="admin_address">' . __('Admin Address', 'admin_address') . '</label>', 'admin_address_html', 'general');
}

function admin_phone_html()
{
    $value = get_option('admin_phone', '');
    echo '<input type="text" id="admin_phone" name="admin_phone" value="' . $value . '" />';
}

function admin_address_html()
{
    $value = get_option('admin_address', '');
    echo '<textarea  id="admin_address" name="admin_address">' . $value . '</textarea>';
}


class BK_Walker_Top_Nav_Menu extends Walker_Nav_Menu
{
    public function start_lvl(&$output, $depth = 0, $args = [])
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"dropdown\">\n";
    }

    public function end_lvl(&$output, $depth = 0, $args = [])
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }
}
class BK_Walker_Social_Nav_Menu extends Walker_Nav_Menu
{
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;
        $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $class_names .'>';

        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title .'': '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';
        $atts['class']  = ! empty( $item->class )        ? $item->class        : 'social-icon';

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $title = apply_filters( 'the_title', $item->title, $item->ID );
        $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

        $icon = strtolower(str_replace(' ','-',$title));

        $item_output = $args->before;

        $item_output .= '<a'. $attributes .'>';

        $item_output .= '<i class="fa fa-'.$icon.'"></i>';
        $item_output .= $args->link_before . $title . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $output .= "</li>{$n}";
    }
}
add_filter( 'wp_nav_menu_items', 'logo_custom_menu_item', 10, 10 );

function logo_custom_menu_item ( $items, $args ) {
    if ($args->theme_location == 'top') {
    $logoImageUrl = get_theme_file_uri('images/bk-logo.png');
        $custom_logo_id = get_theme_mod('custom_logo');
        if($custom_logo_id){
            $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            $logoImageUrl = $logo[0];
        }
    $logoMenuItem = '<a href="/"><img src="'.$logoImageUrl.'" alt="bk" class="img-responsive header-logo"></a>';
        $items = '<li>'.$logoMenuItem.'</li>'.$items;
    }
    return $items;
}
function theme_prefix_setup() {

    add_theme_support('custom-logo', array(
        'height'      => 41,
        'width'       => 106,
        'flex-width' => true,
    ) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );

function get_post_by_slug($slug){
    $posts = get_posts(array(
        'name' => $slug,
        'posts_per_page' => 1,
        'post_type' => 'project',
        'post_status' => 'publish'
    ));

    return $posts[0];
}

function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyAdMFqjdmlqeRqQH161zeuRN3kJQpSCJVs';
	
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

@ini_set('upload_max_size','1024M');
@ini_set('post_max_size','1024M');
@ini_set('max_execution_time','1000');

?>

