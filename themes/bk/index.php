<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
			<?php
			$count = 0;
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					$count++;
					$pageSlug = basename(get_permalink());
					$post = get_post_by_slug($pageSlug);
					if(!empty($post) && $count == 1){
						get_template_part( 'template-parts/post/projects/content', 'project');
					}else{
						get_template_part( 'template-parts/post/content', 'home');
						break;
					}
				endwhile;
			else :
				get_template_part( 'template-parts/post/content', 'none' );
			endif;
			?>
<?php get_footer();
