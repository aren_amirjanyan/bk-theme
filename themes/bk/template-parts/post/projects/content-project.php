<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 19.05.2017
 * Time: 17:26
 */
 //echo '<pre>';print_r($post);

$featured_image_url = get_theme_file_uri('images/what-we-do.jpg');

if (get_field('featured_image', $post->ID)) {
$featured_image = get_field('featured_image', $post->ID);
$featured_image_url = $featured_image['url'];
}
?>
<div class="page-content content-no-cover">
    <div class="container">
        <h1 class="title-secondary"><?php echo $post->post_title; ?></h1>
        <div class="project-details-box">
        <div class="featured-image">
            <img src="<?php echo $featured_image_url;?>" class="img-responsive"/>
        </div>
        <div class="project-details">
            <div class="project-details-section">
                <div class="project-details-info">
                    <p>
                        <span class="project-client-name">Client - </span> 
                        <strong><?php echo get_field('client',$post->ID);?></strong>
                    </p>
                    <p>
                        <span class="project-service-name">Service - </span> 
                        <strong><?php echo get_field('service',$post->ID);?></strong>
                    </p>
                    <p>
                      
                       <?php echo strip_tags($post->post_content);?>
                    </p>
                </div>
            </div>
        </div>
         </div>
    </div>
</div>

