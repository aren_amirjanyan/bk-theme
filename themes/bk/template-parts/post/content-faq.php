<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */

?>
<div class="faq-box">
    <?php $args = array(
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC',
        'post_type' => 'faq',
        'post_status' => 'publish',
    );
    $getAlllFaq = get_posts($args);

    if (!empty($getAlllFaq)) {
        ?>
        <h2 class="title-secondary">F.A.Q</h2>
        <?php
        foreach ($getAlllFaq as $key => $faq) {
            ?>
            <div class="faq-inner">
                <h3 class="title-sm"><?php echo strip_tags(get_field('question', $faq->ID)); ?></h3>
                <p class="faq-text"><?php echo strip_tags(get_field('answer', $faq->ID)); ?></p>
            </div> <?php
        }
    } ?>
</div>
    <?php $args = array(
        'posts_per_page'   => 5,
        'offset'           => 0,
        'category'         => 'Faq',
        'category_name'    => 'faq',
        'orderby'          => 'date',
        'order'            => 'ASC',
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );

$getAllPosts = get_posts($args);
if(!empty($getAllPosts)){
    foreach($getAllPosts as $key=> $value){
    ?>
    <h1 class="lth-title-secondary"><?php echo $value->post_title; ?></h1>
    <div class="looking-to-hire-section"><?php echo $value->post_content; ?></div>
    <?php
    }
}
?>
<?php $args = array(
    'orderby' => 'date',
    'order' => 'ASC',
    'post_type' => 'wpdmpro',
    'post_status' => 'publish',
    'tax_query' => array(
        array(
            'taxonomy' => 'wpdmcategory',
            'field' => 'slug',
            'terms' => 'calendar',
        )
    )
);

$getDownloadableFiles = get_posts($args);

?>
<div class="rdo-box">
    <h2 class="title-secondary">Download forms </h2>

    <div class="input-group">
        <select class="rdo-select input-field" id="download-pdf-files">
            <option vlaue="#">Choose PDF File</option>
            <?php if (!empty($getDownloadableFiles)) {
                foreach ($getDownloadableFiles as $key => $pdffile) {
                    ?>
                    <option
                        value="<?php echo \WPDM\Package::getDownloadURL($pdffile->ID); ?>"><?php echo $pdffile->post_title; ?></option>
                    <?php
                }
            } ?>
        </select>
        <a class="brown-btn" href="#" target="_self" id="download-printable-pdf">DOWNLOAD PRINTABLE PDF</a>
    </div>
</div>