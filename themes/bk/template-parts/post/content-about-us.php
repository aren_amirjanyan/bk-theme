<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */

?>
<?php $args = array(
    'sort_order' => 'desc',
    'sort_column' => 'post_title',
    'hierarchical' => 1,
    'child_of' => 0,
    'parent' => -1,
    'offset' => 0,
    'post_type' => 'page',
    'post_status' => 'publish'
);
$getAllPages = get_pages($args);

$pages = get_page_children( get_the_ID(), $getAllPages);

if (!empty($pages)) {
    foreach ($pages as $key => $page) {
        ?>
        <h1 class="title-secondary"><?php echo $page->post_title; ?></h1>
        <p class="site-txt"><?php echo $page->post_content; ?></p>
    <?php
    }
}
?>
