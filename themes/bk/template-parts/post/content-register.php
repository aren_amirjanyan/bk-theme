<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */

?>
    <?php $args = array(
        'posts_per_page'   => 5,
        'offset'           => 0,
        'category'         => 'Looking for Work',
        'category_name'    => 'looking-for-work',
        'orderby'          => 'date',
        'order'            => 'ASC',
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );

$getAllPosts = get_posts($args);
if(!empty($getAllPosts)){
    foreach($getAllPosts as $key=> $value){
    ?>
    <h1 class="lth-title-secondary"><?php echo $value->post_title; ?></h1>
    <div class="looking-to-work-section"><?php echo $value->post_content; ?></div>
    <?php
    }
}
?>
<div class="lokking-to-hire-form" id="lokking-to-hire-form">
    <?php echo do_shortcode('[contact-form-7 id="61" title="Register for work"]');?>
</div>