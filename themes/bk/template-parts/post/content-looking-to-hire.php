<?php
/**
 * Template part for displaying gallery posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */
?>
    <?php $args = array(
        'posts_per_page'   => 5,
        'offset'           => 0,
        'category'         => 'Looking to Hire',
        'category_name'    => 'looking-to-hire',
        'orderby'          => 'date',
        'order'            => 'ASC',
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );

$getAllPosts = get_posts($args);
if(!empty($getAllPosts)){
    foreach($getAllPosts as $key=> $value){
    ?>
    <h1 class="lth-title-secondary"><?php echo $value->post_title; ?></h1>
    <div class="looking-to-hire-section"><?php echo $value->post_content; ?></div>
    <?php
    }
}
?>
<div class="services-page-box">
    <div class="container">
        <div class="services-row">
            <?php $args = array(
                'posts_per_page' => -1,
                'orderby' => 'date',
                'order' => 'ASC',
                'post_type' => 'service',
                                'tax_query' => array(
                                    array(
                                           'taxonomy' => 'service_category',
                                           'field' => 'slug',
                                           'terms' => 'default',
                                         )
                                   ),
                'post_status' => 'publish',
            );
            $getServices = get_posts($args);
            if (!empty($getServices)) {
                foreach ($getServices as $key => $service) {
                    $featured_image_url = get_theme_file_uri('images/services/civil-labour.png');
                    if (get_field('featured_image', $service->ID)) {
                        $featured_image = get_field('featured_image', $service->ID);
                        $featured_image_url = $featured_image['url'];
                    } ?>
                    <div class="services">
                        <div class="services-inner">
                            <span class="services-icon">
                                <img src="<?php echo $featured_image_url; ?>"
                                     alt="Civil labour" class="img-responsive">
                            </span>
 
                            <h3 class="services-title"><?php echo $service->post_title ?></h3>
                        </div>
                    </div>
                    <?php
                }
            } ?>
        </div>
    </div>
</div>
<div class="lokking-to-hire-form">
     <?php $args = array(
    'sort_order' => 'asc',
    'sort_column' => 'post_title',
    'hierarchical' => 1,
    'child_of' => 0,
    'parent' => -1,
    'offset' => 0,
    'post_type' => 'page',
    'sort_column'  => 'menu_order',
    'post_status' => 'publish'
    );
    $getAllPages = get_pages($args);

$pages = get_page_children(get_the_ID(), $getAllPages);

if (!empty($pages)) {
    foreach ($pages as $key => $page) {
        ?>
        <h2 class="lth-title-secondary"><?php echo $page->post_title; ?></h2>
        <p class="site-txt"><?php echo $page->post_content; ?></p>
    <?php
    }
}
?>
    <?php echo do_shortcode('[contact-form-7 id="60" title="Inquiry"]'); ?>
</div>
