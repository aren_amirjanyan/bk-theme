<?php
/**
 * Template part for displaying posts with excerpts
 *
 * Used in Search Results and for Recent Posts in Front Page panels.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */

?>
<h1 class="title-secondary"><?php the_title(); ?> </h1>
<div id="news" class="latest-news-box">
    <div class="container">
        <div class="latest-project">
            <?php $args = array(
                'posts_per_page' => -1,
                'orderby' => 'date',
                'order' => 'ASC',
                'post_type' => 'project',
                'post_status' => 'publish',
            );
            $getProjects = get_posts($args);

            if (!empty($getProjects)) {
                foreach ($getProjects as $key => $project) {
                    ?>
                    <div class="single-project">
                        <div class="single-project-inner">
                            <div class="project-img">
                                <?php
                                $featured_image_url = get_theme_file_uri('/images/news-img.jpg');
                                if (get_field('featured_image', $project->ID)) {
                                    $featured_image = get_field('featured_image', $project->ID);
                                    $featured_image_url = $featured_image['url'];
                                } ?>
                                <a href="<?php echo $project->guid; ?>"><img src="<?php echo $featured_image_url; ?>"
                                                                             class="img-responsive project-img b-lazy b-loaded"
                                                                             alt="news"/>

                                    <div class="project-title-overlay">
                                        <div class="project-title">
                                            <h2 class="h2-project-title"><?php echo $project->post_title ?></h2>

                                            <p><?php echo (strlen(strip_tags($project->post_content)) > 100) ? substr(strip_tags($project->post_content), 0, 100) . '...' : strip_tags($project->post_content); ?>
                                        </div>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
