<?php
/**
 * Template part for displaying audio posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */
?>
<h1 class="title-secondary">
    <?php the_title(); ?>
</h1>
<p class="site-txt">
    <?php the_content(); ?>
</p>

