<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */
?>
<h1 class="title-secondary"><?php echo the_title(); ?></h1>
<div class="contact-us-form">
    <?php echo do_shortcode('[contact-form-7 id="62" title="Contact Us"]'); ?>
</div>
<div class="addresses">
    <?php $args = array(
        'posts_per_page'   => 5,
        'offset'           => 0,
        'category'         => 'Address',
        'category_name'    => 'address',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );

$getAllPosts = get_posts($args);

if(!empty($getAllPosts)){
    $locations = [];
    foreach($getAllPosts as $key => $post){
        $locations = get_field('locations',$post->ID);
        ?>
        <h1 class="title-secondary"><?php echo $post->post_title; ?></h1>
        <div class="address">
            <p class="address-sub-title"><?php echo get_field('sub_title',$post->ID); ?></p>
            <div class="contact-page-container">
	            <div id="location-map-<?php echo $post->ID;?>" class="location-map" data-id="<?php echo $post->ID;?>"  data-address="<?php echo strip_tags(get_field('address',$post->ID));?>" data-lat="<?php echo $locations['lat'];?>" data-lng ="<?php echo $locations['lng'];?>"></div>
	            <div class="contact-details-section">
		            <div class="contact-details-info">
		                <h3 class="contact-details-title">Contact Details</h3>
		                <div class="contact-details">
		                    <p class="address-title"><?php echo strip_tags(get_field('address',$post->ID)); ?></p>
		                    <p class="address-phone">T <a href="tel:<?php echo strip_tags(get_field('phone',$post->ID)); ?>"><?php echo strip_tags(get_field('phone',$post->ID)); ?></a></p>
		                    <p class="address-fax">F <?php echo strip_tags(get_field('fax',$post->ID)); ?></p>
		                </div>
		            </div>
		           <?php /* ?> <div class="hourse-of-operation-info">
		                <h3 class="hourse-of-operation-title">Hours of operation</h3>
		                <div class="hours-of-operation">
		                    <?php echo strip_tags(get_field('hours_of_operation',$post->ID)); ?>
		                </div>
		            </div> <?php */ ?>
	             </div>
            </div>
        </div>
        <?php
    }
}
?>
</div>

<script type="text/javascript">
    function initMap() {
        var location = jQuery('.location-map');
        if(location.length > 0){
            jQuery.each(location,function(key,value){
                var mapContent = jQuery(value).attr('id');
                var latitude = parseFloat(jQuery(value).attr('data-lat'));
                var longitude = parseFloat(jQuery(value).attr('data-lng'));
                var address = jQuery(value).attr('data-address');
                var myLatLng = {lat: latitude, lng: longitude};
                var map = new google.maps.Map(document.getElementById(mapContent), {
                    center: myLatLng,
                    zoom: 10
                });
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: address
                });
            });
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdMFqjdmlqeRqQH161zeuRN3kJQpSCJVs&callback=initMap" async defer></script>

