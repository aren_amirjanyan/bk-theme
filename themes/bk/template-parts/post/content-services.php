<?php
/**
 * Template part for displaying posts with excerpts
 *
 * Used in Search Results and for Recent Posts in Front Page panels.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */

?>
<?php /*?> <h1 class="title-secondary"><?php the_title(); ?></h1> <?php */ ?>
<div class="services-page-box">
<?php /*?>
    <div class="container">
        <div class="services-row">
            <?php $args = array(
                'posts_per_page' => -1,
                'orderby' => 'date',
                'order' => 'ASC',
                'post_type' => 'service',
                                'tax_query' => array(
                                    array(
                                           'taxonomy' => 'service_category',
                                           'field' => 'slug',
                                           'terms' => 'default',
                                         )
                                   ),
                'post_status' => 'publish',
            );
            $getServices = get_posts($args);
            if (!empty($getServices)) {
                foreach ($getServices as $key => $service) {
                    $featured_image_url = get_theme_file_uri('images/services/civil-labour.png');
                    if (get_field('featured_image', $service->ID)) {
                        $featured_image = get_field('featured_image', $service->ID);
                        $featured_image_url = $featured_image['url'];
                    } ?>
                    <div class="services">
                    	<div class="services-inner">
                            <span class="services-icon">
                                <img src="<?php echo $featured_image_url; ?>"
                                     alt="Civil labour" class="img-responsive">
                            </span>

                            <h3 class="services-title"><?php echo $service->post_title ?></h3>
                        </div>
                    </div>

                    <?php
                }
            }
            ?>
        </div>
    </div>
<?php  */ ?>
<!-- Labour hire services -->

<h1 class="title-secondary">Labour Hire Services</h1>
<p class="site-txt">BK Labour Hire delivers construction labour hire services in Australia. Bring in our workforce for your public or private sector jobs. Call us for flexible staffing for your commercial, residential, and infrastructure projects. We staff casual, temporary, and temp-to-hire people. You get the right people, because we have the worked in the building trades. Contact us for labour hire <a href="http://bklabourhire.com.au/looking-to-hire/">services</a></p>
<div class="services-page-box">
    <div class="container">
        <div class="services-row">
            <?php $args = array(
                'posts_per_page' => -1,
                'orderby' => 'date',
                'order' => 'ASC',
                'post_type' => 'service',
                'tax_query' => array(
                                    array(
                                           'taxonomy' => 'service_category',
                                           'field' => 'slug',
                                           'terms' => 'labour-hire-services',
                                         )
                                   ),
                'post_status' => 'publish',
            );
            $getServices = get_posts($args);
            if (!empty($getServices)) {
                foreach ($getServices as $key => $service) {
                    $featured_image_url = get_theme_file_uri('images/services/civil-labour.png');
                    if (get_field('featured_image', $service->ID)) {
                        $featured_image = get_field('featured_image', $service->ID);
                        $featured_image_url = $featured_image['url'];
                    } ?>
                    <div class="services">
                    	<div class="services-inner">
                            <span class="services-icon">
                                <img src="<?php echo $featured_image_url; ?>"
                                     alt="<?php echo $service->post_title ?>" class="img-responsive">
                            </span>
                            <h3 class="services-title"><a href="#"><?php echo $service->post_content; ?></a></h3>
                        </div>
                        <h3 class="service-portal-title" ><?php echo $service->post_title; ?></h3>
                    </div>

                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
    <div class="container">
<?php $args = array(
    'sort_order' => 'desc',
    'sort_column' => 'post_title',
    'hierarchical' => 1,
    'child_of' => 0,
    'parent' => -1,
    'offset' => 0,
    'post_type' => 'page',
    'post_status' => 'publish'
);
$getAllPages = get_pages($args);

$pages = get_page_children(get_page_by_title('services')->ID, $getAllPages);

if (!empty($pages)) {
    foreach ($pages as $key => $page) {
        ?>
        <h1 class="title-secondary"><?php echo $page->post_title; ?></h1>
        <p class="site-txt"><?php echo $page->post_content; ?></p>
    <?php
    }
}
?>
</div>
</div>
