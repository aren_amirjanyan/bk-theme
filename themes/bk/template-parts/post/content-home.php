<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */

?>

<div class="top-cover">
	<div class="container">
		<h1 class="main-title">Building safe, motivated crews</h1>
		<div class="button-group">
			<a class="orange-btn" href="/looking-to-hire">LOOKING TO HIRE</a>
			<a class="brown-btn" href="/looking-for-work">LOOKING TO WORK</a>
		</div>
	</div>
</div>
<div class="white-content">
	<div class="container">
		<?php $args = array(
		'name'        => 'choose-bk-labour-hire',
		'post_type'   => 'page',
		'post_status' => 'publish',
		'numberposts' => 1
		);
		$chooseBkLabourHire = get_posts($args);
		?>
		<h2 class="title-secondary"><?php echo $chooseBkLabourHire[0]->post_title ?>?</h2>
		<p class="site-txt"><?php echo $chooseBkLabourHire[0]->post_content; ?></p>
		
                <div class="portals">
                <?php $args = array(
				'posts_per_page' => 3,
				'orderby' => 'date',
				'order' => 'ASC',
				'post_type' => 'service',
				'tax_query' => array(
                                    array(
                                           'taxonomy' => 'service_category',
                                           'field' => 'slug',
                                           'terms' => 'labour-hire-home',
                                         )
                                   ),
				'post_status' => 'publish'
			);
			$getServices = get_posts($args);
			if (!empty($getServices)) {

				foreach ($getServices as $key => $service) {
					$featured_image_url = get_theme_file_uri('images/mining-icon.png');
					
					if (get_field('background_image', $service->ID)) {
						$featured_image = get_field('background_image', $service->ID);
						$featured_image_url = $featured_image['url'];
					} 
					?>
					
			<div class="labour-hire-services">
                            <span class="portal-icon">
                                <img src="<?php echo $featured_image_url; ?>" alt="<?php echo $service->post_title; ?>" class="img-responsive">
                            </span>
                                <h3 class="portal-title"><?php echo $service->post_title; ?></h3>
                        </div>
					<?php }
					} ?>
                </div>
	</div>
</div>
<div class="services-box">
	<div class="container">
		<div class="owl-carousel owl-theme">
			<?php $args = array(
				'posts_per_page' => -1,
				'orderby' => 'date',
				'order' => 'ASC',
				'post_type' => 'service',
				'tax_query' => array(
                                    array(
                                           'taxonomy' => 'service_category',
                                           'field' => 'slug',
                                           'terms' => 'default',
                                         )
                                   ),
				'post_status' => 'publish',
			);
			$getServices = get_posts($args);
			if (!empty($getServices)) {
				foreach ($getServices as $key => $service) {
					$featured_image_url = get_theme_file_uri('images/services/civil-labour.png');
					if (get_field('featured_image', $service->ID)) {
						$featured_image = get_field('featured_image', $service->ID);
						$featured_image_url = $featured_image['url'];
					} ?>
					<div class="item">
						<div class="services">
                            <span class="services-icon">
                                <img src="<?php echo $featured_image_url; ?>" alt="<?php echo $service->post_title ?>" class="img-responsive">
                            </span>
							<h3 class="services-title"><?php echo $service->post_title ?></h3>
						</div>
					</div>

					<?php
				}
			}
			?>
		</div>
	</div>
</div>
<div class="white-content">
	<div class="container">
		<?php $args = array(
			'name'        => 'our-mission',
			'post_type'   => 'page',
			'post_status' => 'publish',
			'numberposts' => 1
		);
		$ourMission = get_posts($args);
		?>
		<h2 class="title-secondary"><?php echo $ourMission[0]->post_title; ?></h2>
		<p class="site-txt"><?php echo $ourMission[0]->post_content; ?></p>
	</div>
</div>
<div class="grey-bg">
	<div class="container">
		<?php $args = array(
			'name'        => 'bk-labour-hire',
			'post_type'   => 'page',
			'post_status' => 'publish',
			'numberposts' => 1
		);
		$bkLabourHire= get_posts($args);
		?>
		<h2 class="title-secondary"><?php echo $bkLabourHire[0]->post_title; ?>?</h2>
		<p class="site-txt"><?php echo $bkLabourHire[0]->post_content; ?></p>
	</div>
</div>
<div class="white-content">
	<div class="container">
		<?php $args = array(
		'name'        => 'why-choose-us',
		'post_type'   => 'page',
		'post_status' => 'publish',
		'numberposts' => 1
		);
		$whyChooseUs = get_posts($args);
		?>
		<h2 class="title-secondary"><?php echo $whyChooseUs[0]->post_title ?>?</h2>
		<p class="site-txt"><?php echo $whyChooseUs[0]->post_content; ?></p>
	</div>
</div>
<div class="testimonial-box testimonial-box-home">
	<div class="container">
		<!--<p class="testimonial-txt">
			Have used www.bklabourhire.com.au for several years now, and have always been incredibly happy with
			the professional service I have received.
		</p>
		<p class="testimonial-user-name">
			- Mark
		</p>-->
	</div>
</div>
