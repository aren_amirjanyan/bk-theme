<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<p class="copyright-text"><?php printf( __( 'COPYRIGHT %s', 'bk' ), '2017' ); ?>  | ALL RIGHTS RESERVED | POWERED BY WORDPRESS</p>
</div><!-- .site-info -->
