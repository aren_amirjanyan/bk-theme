<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.2
 */

?>
<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'bk' ); ?>">
	<?php wp_nav_menu( array(
		'theme_location' => 'top',
		'menu_id'        => 'top-menu',
		'menu_class' => 'main-menu',
		'walker' => new BK_Walker_Top_Nav_Menu()
	) ); ?>
</nav><!-- #site-navigation -->
