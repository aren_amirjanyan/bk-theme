<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.0
 */

get_header();

$pageSlug = basename(get_permalink());

if ($pageSlug === 'home') {
    ?>
    <div class="page-content">
        <?php
        if (have_posts()) :
            while (have_posts()) : the_post();
                get_template_part('template-parts/post/content', $pageSlug);
            endwhile;
        else :
            get_template_part('template-parts/post/content', 'none');
        endif;
        ?>
    </div>
    <?php
} else {

    ?>
    <div class="page-content content-no-cover">
        <div class="container">
            <?php
            if (have_posts()) :
                while (have_posts()) : the_post();
                    get_template_part('template-parts/page/content', 'page');
                endwhile;
            endif;
            ?>
        </div>
    </div>
    <?php if($pageSlug === 'about-us' || $pageSlug === 'who-we-are'){ ?>
        <div class="testimonial-box testimonial-box-about-us"></div>
    <?php
    }elseif($pageSlug === 'who-chooses-us'){
    ?>
    	<div class="testimonial-box testimonial-box-who-chooses-us"></div>
    <?php
    }elseif($pageSlug === 'safety'){
    ?>
    	<div class="testimonial-box testimonial-box-safety"></div>
    <?php
    }elseif($pageSlug === 'labour-hire-services' || $pageSlug === 'services'){
    ?>
    	<div class="testimonial-box testimonial-box-labour-hire-services"></div>
    <?php
    }elseif($pageSlug === 'looking-to-hire'){
    ?>
    	<div class="testimonial-box testimonial-box-looking-to-hire"></div>
    <?php
    }elseif($pageSlug === 'looking-for-work'){
    ?>
    	<div class="testimonial-box testimonial-box-looking-for-work"></div>
    <?php
    }elseif($pageSlug === 'register'){
    ?>
    	<div class="testimonial-box testimonial-box-register"></div>
    <?php
    }elseif($pageSlug === 'faq'){
    ?>
    	<div class="testimonial-box testimonial-box-faq"></div>
    <?php
    }elseif($pageSlug === 'contact-us'){
    ?>
    	<div class="testimonial-box testimonial-box-contact-us"></div>
    <?php
    }


}
?>
<?php get_footer();