<?php
/**
 * The header for BK theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage BK
 * @since 1.0
 * @version 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"/>
    <title><?php wp_title();?></title>
    <meta name="description" content="Choose BK Labour Hire for Australian construction trades you need now. Get competent casual, temp, temp-to-hire. We work with skilled labour, licensed operators & ticketed labourers.">
    <meta name="keywords" content="BK Labour Hire">
    <?php wp_head(); ?>
</head>
<body>
<div class="page-wrap">
    <div class="top-info-box">
        <div class="container">
            <?php  $logoImageUrl = get_theme_file_uri('images/middle-logo.png');
                   /*$custom_logo_id = get_theme_mod('custom_logo');
                   if($custom_logo_id){
                        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                        $logoImageUrl = $logo[0];
                   }*/
                  $logoMenuItem = '<a href="/"><img src="'.$logoImageUrl.'" alt="bk" class="img-responsive header-logo"></a>';
?>
                <div class="header-top-logo"><?php echo $logoMenuItem ?></div>
            <ul class="top-info-list">
                <li class="info-phone">
                   <a href="tel:<?php echo get_option('admin_phone'); ?>"><?php echo get_option('admin_phone'); ?></a>
                </li>
                <li class="info-email">
                    <a href="mailto:<?php echo get_option('admin_email');?>"><?php echo get_option('admin_email');?></a>
                </li>
                <li class="info-place">
                 <a href="http://maps.google.com/?q=<?php echo get_option('admin_address');?>" target="_blank"><?php echo get_option('admin_address');?></a>
                </li>
            </ul>
        </div>
        <div class="container">
            <header>
                <?php if ( has_nav_menu( 'top' ) ) : ?>
                    <div class="navigation-top">
                        <div class="wrap">
                            <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                        </div><!-- .wrap -->
                    </div><!-- .navigation-top -->
                <?php endif; ?>
            </header>
        </div>
    </div>
